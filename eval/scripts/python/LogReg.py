#!/usr/bin/env python3

'''
KST2Side.py

Simple script to evaluate agreement between continuous, nonparameterized datasets using two-sided Kolmogorov-Smirnov test.

Rhys Braginton Pettee Olsen
Boulder, Colorado
2017-02-10
'''

import csv
import math
import random
from scipy import special, stats

from sklearn import linear_model, model_selection, tree

import numpy as np
import matplotlib.pyplot as plt

# F_{Gauss(mu, sigma_sqr)}^{-1}(x)
def gauss_inv(x, mu, sigma_sqr):
    return mu - math.sqrt(2.0 * sigma_sqr) * special.erf((2.0 * x) - 1.0)

def generate_gaussian(mu, sigma_sqr):
    return gauss_inv(random.random(), mu, sigma_sqr)

def main():
    correct_ranks   = []
    incorrect_ranks = []
    
    f = open('../../results/joined_stripped.txt', 'r')
    try:
        reader = csv.DictReader(f, delimiter=" ")
        for row in reader:
            if row["correct"] is '1':
                # print("Boom!")
                correct_ranks.append(float(row["rank"]))
            elif row["correct"] is '0':
                # print("Bust!")
                incorrect_ranks.append(float(row["rank"]))
            # else:
                # print("Bah!")
    finally:
        f.close()

    x = np.array(correct_ranks + incorrect_ranks)
    X = np.array([np.array([i]) for i in x])
    y = np.array([1 for i in range(0,len(correct_ranks))] + [0 for i in range(0,len(incorrect_ranks))])

    lr = linear_model.LogisticRegression(penalty='l1')
    
    kf = model_selection.KFold(n_splits=6, shuffle=True)

    dt = tree.DecisionTreeClassifier(max_depth=1)

    precisions = []
    recalls = []
    i = 0
    for train_index, test_index in kf.split(X,y):
        x_train = x[train_index]
        X_train = [[i] for i in x_train]
        y_train = y[train_index]

        x_test = x[test_index]
        X_test = [[i] for i in x_test]
        y_test = y[test_index]

        dt.fit(X_train, y_train)
        y_predict = dt.predict(X_test)
        tree.export_graphviz(dt, out_file="tree"+str(i)+".dot")
        
        true_pos  = 0
        false_pos = 0

        true_neg  = 0
        false_neg = 0

        pred_pos = 0
        test_pos = 0
        
        for i in range(0, len(y_test)):
            if y_test[i]:
                test_pos = test_pos + 1
            if y_predict[i]:
                pred_pos = pred_pos + 1
                if y_test[i]:
                    true_pos = true_pos + 1
                else:
                    false_pos = false_pos + 1
            else:
                if y_test[i]:
                    false_neg = false_neg + 1
                else:
                    true_neg = true_neg + 1
        
        precision = float(true_pos) / float(pred_pos)
        recall    = float(true_pos) / float(test_pos)

        precisions.append(precision)
        recalls.append(recall)
        '''
        lr.fit(
            X_train, y_train
        )
        '''
        i = i + 1
    print("Precisions\tRecalls")
    print(precisions, "\t", recalls)
    print("Mean Precision\tMean Recall")
    print(np.mean(precisions), np.mean(recalls))
    print("SD Precision\tSD Recall")
    print(np.std(precisions), np.std(recalls))
    lr = linear_model.LogisticRegression(penalty='l1')
    lr.fit(
        X_test,y_test
    )
    #print(lr.get_params())
    
    # print(std_normals_1)
    # print(std_normals_2)
    # print(supstd_normals_2)

    # test_1_2 = stats.ks_2samp(std_normals_1, supstd_normals_2)
    # print(test_1_2)
    return 0

if __name__ == "__main__":
    main()
