#!/usr/bin/env python3

'''
KST2Side.py

Simple script to evaluate agreement between continuous, nonparameterized datasets using two-sided Kolmogorov-Smirnov test.

Rhys Braginton Pettee Olsen
Boulder, Colorado
2017-02-10
'''

import csv
import math
import random
from scipy import special, stats

from sklearn import linear_model

import numpy as np
import matplotlib.pyplot as plt

# F_{Gauss(mu, sigma_sqr)}^{-1}(x)
def gauss_inv(x, mu, sigma_sqr):
    return mu - math.sqrt(2.0 * sigma_sqr) * special.erf((2.0 * x) - 1.0)

def generate_gaussian(mu, sigma_sqr):
    return gauss_inv(random.random(), mu, sigma_sqr)

def main():
    correct_ranks   = []
    incorrect_ranks = []
    
    f = open('../../results/joined_stripped.txt', 'r')
    try:
        reader = csv.DictReader(f, delimiter=" ")
        for row in reader:
            if row["correct"] is '1':
                # print("Boom!")
                correct_ranks.append(float(row["rank"]))
            elif row["correct"] is '0':
                # print("Bust!")
                incorrect_ranks.append(float(row["rank"]))
            # else:
                # print("Bah!")
    finally:
        f.close()
    std_normals_1 = [generate_gaussian(0, 1) for i in range(0, 10)]
    std_normals_2 = [generate_gaussian(0, 1) for i in range(0, 10)]
    supstd_normals_2 = [generate_gaussian(0, 12) for i in range(0, 200)]

    # print(correct_ranks)
    # print(incorrect_ranks)
    
    '''
    Remember your axes, Rhys! This is your CDF:
     ^ proportion of ACDFGs (CDF)
    1+       _,-
     | __,--'
     |/
    0|_________, PageRank / centrality measure
               '
    '''
    
    test_popularity = stats.ks_2samp(correct_ranks, incorrect_ranks)
    print(test_popularity)

    #diagram_support = range(math.floor(min(correct_ranks)), math.ceil(max(correct_ranks)))
    diagram_support = [float(x) / 10.0 for x in range(0,10)]
    # plt.hist(correct_ranks, normed=True, color='b', align='left', bins = diagram_support)
    colors = ['blue', 'red']
    labels = ['Correct', 'Incorrect']
    plt.hist([[correct_ranks], incorrect_ranks], normed=1, histtype='step', fill=False, stacked=False, color=colors, label=labels, align='left', bins=diagram_support)
    axes = plt.gca()
    axes.set_xlim(0, 1)
    # axes.set_ylim(0, 1)
    plt.legend(prop={'size': 10})
    plt.show()

    lr = linear_model.LogisticRegression(penalty='l1')
    lr.fit(
        [[i] for i in correct_ranks] + [[i] for i in incorrect_ranks],
        [1 for i in range(0,len(correct_ranks))] + [0 for i in range(0,len(incorrect_ranks))]
    )
    print(lr.get_params())
    
    # print(std_normals_1)
    # print(std_normals_2)
    # print(supstd_normals_2)

    # test_1_2 = stats.ks_2samp(std_normals_1, supstd_normals_2)
    # print(test_1_2)
    return 0

if __name__ == "__main__":
    main()
