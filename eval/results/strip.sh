#!/bin/sh
awk -F '\t' '{ if ($2 != "-1") print $1 " " $2 }' exp_label.tsv > exp_stripped.txt

awk -F ', ' '{ print $2 " " $3 " " $4 }' communities.txt | sort -n > communities_stripped.txt

join communities_stripped.txt exp_stripped.txt > joined_stripped.txt
