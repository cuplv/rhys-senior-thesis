\begin{savequote}[112mm]
  For a greasy little nobody, you do have good bone structure.
\qauthor{Heather Chandler, \emph{Heathers: The Musical}}
\end{savequote}

\chapter{Experiment}
\label{chap:experiment}

\section{Experimental Setup}

\newthought{For us to study} the popular is beautiful hypothesis in an objective and readily repeatable fashion using our notion of a \groum{} embedding and \cmg{}, we picked a particular Android \api{} usage example with a simple, well-understood pattern of correct usage. This usage pattern queries a pre-existing \texttt{SQLiteDatabase} through calls to a helper object called \texttt{cursor} created from the database instance before calling \texttt{cursor.close()}~\cite{androiddoc/cursor,stackoverflow/cursor1}. The most basic form of this correct usage is shown in figure~\ref{fig:basic_usage}, though more elaborate usages may also be correct, both in terms of implementing other functionality or in terms of dealing with other misuses of the \api{}, as we shall see.

There are a number of ways this pattern can go wrong. In the most basic case, the developer can fail to invoke \texttt{cursor.close()}, which causes the \texttt{cursor} object to remain attached to the database by the system,  leaking a system resource and raising a \texttt{DatabaseObjectNotClosedException} exception~\cite{stackoverflow/cursor1,stackoverflow/cursor2}, which if not handled will crash the app. For our purposes, this constitutes a misuse of the \api{} and is always incorrect behavior. We note that in practice, a number of developers wrap calls to \texttt{db.query()} or \texttt{cursor.close()} in helper methods to supply a different \api{} for the same functionality. Then the overall usage, which includes invocation of these helper methods, may or may not be correct. In any event, we treat these helper method cases as misuses: even though they may be used to realize correct \api{} usage, the onus of responsibility to do so remains with whoever calls them (they can still be misused), so they do not constitute usage that is correct and idiomatic in isolation, and our intraprocedural analysis should recognize them as suspicious for this reason.

A more complicated problem can arise, however. If the \texttt{db} object does not exist or is somehow errant during the constructor call to \texttt{db.query()}, the method invocation will fail to create the \texttt{cursor}, instead throwing an \texttt{IllegalStateException}\cite{stackoverflow/illegalstateexception}. In cases where this is liable to happen, this exception must be caught, lest the problem propagate down the control stack, possibly crashing the entire app. This behavior depends on the interactions between the call to \texttt{db.query()} and its state, which may implicate stored databases or other procedures in the app itself or library or system code. As this potentially very complicated issue requires full interprocedural program analysis, possibly including some kind of interevent modeling, it is readily outside of the scope of this thesis' investigation of correct intraprocedural \api{} usage patterns and so is not further studied here with reference to our corpus. We simply note that gracefully handling this case require a defensive elaboration of the most basic usage pattern into a \texttt{try}-\texttt{catch}-\texttt{finally} control flow form with a null checks on \texttt{cursor} (shown in figure~\ref{fig:complex_usage})\cite{stackoverflow/cursor1}, which while correct both in behavior and for our purposes isn't neccessary assuming the \texttt{db} object is correctly instantiated.

\begin{figure}[H]
\begin{lstlisting}[language=Java]
  // SQLiteDatabase db = SQLiteDatabase.getWritableDatabase();
  Cursor cursor = db.query();
  /*
   * perform database actions using cursor...
   */
   cursor.close();
\end{lstlisting}
\caption{Basic code pattern exhibiting correct usage of the \texttt{cursor} object present in the wild.}
\label{fig:basic_usage}
\end{figure}

\begin{figure}[H]
\begin{lstlisting}[language=Java]
  try {
    // SQLiteDatabase db = SQLiteDatabase.getWritableDatabase();
    Cursor cursor = db.query();
    /*
     * perform database actions using cursor...
     */
  }
  catch (SQLiteException e) {
  }
  finally {
    if (cursor != null) {
      cursor.close();
    }
  }
\end{lstlisting}
\caption{More elaborate code pattern featuring defensive \texttt{try}-\texttt{catch} form and a (typically dead) \texttt{null} value-check exhibiting correct usage of the \texttt{cursor} object present in the wild.}
\label{fig:complex_usage}
\end{figure}

This usage pattern is attractive for study because despite its apparent simplicity and the ease with which instances can be classified as correct or incorrect, the behavior it can exhibit is surprisingly subtle, and an array of variations on the basic pattern are correct, while subtle changes or omissions to it can destroy this correctness. As such, it is a meaningful test of the robustness of our approach and the factors our approach can identify. Moreover, we found numerous misuses of this pattern in the wild, some of which were pernicious. For example, one misuse that we classified followed \texttt{db.query()} with a branch condition that was almost always taken, with two branches that featured elaborate control flow but with only the most commonly executed branch properly calling \texttt{cursor.close()}. This presumably mistaken usage results in execution that can be errant, even though this errancy is rarely exhibited.

In chapter~\ref{chap:theory}, we formally posed the \groum{}, embedding relation (and maximal embedding), and community metagraph (\cmg{}). For this experiment, we used a pipeline developed as part of our previous work that efficiently performs \groum{} extraction, \groum{} embedding, and \cmg{} generation, using filtering techniques based on compatible node and edge-counting to limit the number of embeddings that need to be computed ~\cite{fse17}. The embedding we developed can be looked at as a form of $\mathcal{E}^{\text{max}}_{N,S}$, where method signature and object type but not command exactly constraints and no totality constraints are imposed. The embedding deviates from our theoretical form in one subtle way: method signatures that are not exactly equivalent can still be matched, but a numerical penalty is associated with each mismatch between a method argument or a method reciever and its type, and the numerical penalty is minimized to keep the mismatch as minute as possible. A score is calculated from the weighted number of matching nodes and edges, and the penalty is subtracted from this. This enables us to treat more \groum{}s for experiments like the one that follows, and in practice, it does not significantly alter the embeddings that result from the computation. The score is used to assign edge weights to \groum{}s in the community metagraph. Finding a suitable weighting for the the score involves choosing parameters, which could in principle be done from the data collected from this experiment, although it would neccessitate recomputing the \cmg{}. We were able to obtain a useful \cmg{} by weighting all rewards equally and making the penalty associated with a mismatch a fraction of the reward for matching a method invocation.

The automated extraction proceeded as follows: we ingested a large corpus of approximately one quarter-million open source Android app repositories from GitHub and took from these a subset of approximately 10\% of the whole, of which we isolated and prepared \groums{} for precisely those 329 methods utilizing \texttt{db.query()} in some way. This can be thought of a minimal usage of the \texttt{cursor} pattern. All fruitful graph embeddings were performed pairwise, and the community meta-graph was generated for these .The PageRank $R(G_i)$ was then computed for each \groum{} $G_i$. PageRank is a measure of centrality/connectedness of a node in a weighted graph~\cite{ilprints422}, well known for being the algorithm on which the popular search engine Google was originally based. Because PageRank is easy to compute using off-the-shelf tools, can be described in terms of connectedness events under our graph model, and is an effective statistic for measuring how ``prominant'' or ``important'' a given node is in a network, it was selected as our notion of ``popular''. In practice, a variety of graph centrality measures could have been used instead or in addition.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{figures/right.eps}
  \caption{A \groum{} found as part of this experiment exhibiting relatively simple control flow and correct usage of the kind shown in figure~\ref{fig:basic_usage}. Control flow is black, def edges are blue, and use edges are red, as is the case in figure~\ref{fig:wrong}.}
  \label{fig:right}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=.7\linewidth]{figures/wrong.eps}
  \caption{A \groum{} found as part of this experiment exhibiting elaborate control flow and the perniciously wrong usage where \texttt{cursor.close()} is only present on one of two branches.}
  \label{fig:wrong}
\end{figure}


\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{figures/embed.eps}
  \caption{An embedding between two \groum{}s collected as part of the experiment. The embeding itself is drawn with red lines, while control flow is black, def edges are blue, and use edges are green. Only matching elements in each \groum{} are shown, and the first of the two \groum{}s is boxed.}
  \label{fig:embed}
\end{figure}

100 of the 329 methods were manually triaged, of which 62 were labelled as being correct (an example of which is shown in figure~\ref{fig:right}), while 36 were labelled as being incorrect (an example of which exhibiting the subtly wrong behavior described earlier is shown in figure~\ref{fig:wrong}). The remaining 2 were found to be inadmissible for the statistical evaluation though were still included in the community meta-graph: their usage of the \api{} was very complicated and involved interaction with many custom data structures, so they were not comparable to our baseline pattern as correct or incorrect. An example of an embedding computed as part of this experiment is shown in figure~\ref{fig:embed}. The empirical probability density function of the PageRank appears in figure~\ref{fig:pdf}.

\begin{figure}[H]
  \centering
  \includegraphics{figures/query_centrality.eps}
  \caption{Empirical probability density function, with density normalized to bin size, of Page\allowbreak{}Rank for popular (blue) and unpopular (red) examples. PageRank is normed by construction.}
  \label{fig:pdf}
\end{figure}

\section{Experimental Results}

The first and most coarse experiment we performed was an equality-of-distribution test (two-sided Kolmogorov-Smirnov) for the distributions of PageRank of the popular and unpopular samples. Under the null hypothesis $H_0$ (the hypothesis~\ref{hyp:weak_null} of chapter~\ref{chap:question}), there is no significant difference in the distribution of PageRank (``popularity'') with respect to correctness (``beauty''). Our alternative hypothesis $H_1$ is that correct and incorrent usages differ significantly in PageRank distribution (hypothesis~\ref{hyp:weak} of chapter~\ref{chap:question}). Note that no hypotheses apart from these are possible.

The $p$-value for the correct and incorrect usages having the same PageRank distributions for this sample size is $4.7\times10^{-5}$: it is extraordinarily unlikely that these two kinds of usage have the same popularity distribution, and we can reject $H_0$ in favor of $H_1$ for any plausible choice of $\alpha$. (The two-sided Kolmogorov-Smirnov test is non-parametric and assumes only that PageRank is a continuous random variable, which is true by construction).

In light of these results, we strongly reject the weak null hypothesis (hypothesis~\ref{hyp:weak_null}) in favor of the weak hypothesis (hypothesis~\ref{hyp:weak}).

Next, we trained and evaluated two common statistical models---logistic regression, which is a parametric continuous model used to approximate binary outputs using a continuous sigmoid function, and a decision tree classifier, which is a discrete, nonparametric model used to represent categorical outputs---with respect to our data set, using PageRank as our observed variable $x \in [0,1]$ and correctness as our target variable $y \in \{0, 1\}$.

Using $k$-fold model validation (for $k$ = 6), we get $0.81\pm0.14$ and $0.83\pm0.11$ for precision and recall, respectively, with a decision tree; and $0.63\pm0.08$ and $1.0$ for precision and recall, respectively, with logistic regression, predicting correctness from popularity. What this tells us is that correctness is not monotonic in popularity but rather that correct usages tend to occur in especially connected or isolated segments of the population as a whole, though, to our surprise, \emph{most} of the correct usages were rather isolated, such that the decision seperator consistently set an \emph{exceptionally low} PageRank as a predictor of correctness. Incorrect usages, by contrast, seem to be very well concentrated in the somewhat unpopular range, with their popularities tapering off faster than those of the correct usages. To put matters crudely, the most distinctive feature of incorrect usages for this example are their \emph{mediocre} popularity.

From this, we must reject the strong form of the popular is beautiful hypothesis (hypothesis~\ref{hyp:strong}) that a high PageRank significantly predicts correctness in favor of retaining its negation, the alternative strong hypothesis (hypothesis~\ref{hyp:strong_alt}). However, as we have seen that the PageRank distribution does give useful information about correctness, and further that a binary seperator \emph{is} effective in predicting correctness from PageRank under validation, we can reject the strong null hypothesis (hypothesis~\ref{hyp:strong_null}).
