\newcommand\scrC{\mathcal{C}}
\newcommand\scrP{\mathcal{P}}
\newcommand\scr[1]{\mathcal{#1}}
\newcommand\mathsc[1]{\mbox{\textsc{#1}}}
\newcommand\compNodes{\mathsc{compNodes}}
\newcommand\compEdges{\mathsc{compEdges}}
\newcommand\exactlyOne{\mathsc{exactlyOne}}
\newcommand\atleastOne{\mathsc{atleastOne}}
\newcommand\atmostOne{\mathsc{atmostOne}}

\begin{savequote}[112mm]
The only thing worse than being talked about is not being talked about.
\qauthor{Lord Henry Wotton, \emph{The Picture of Dorian Gray} by Oscar Wilde}
\end{savequote}

\newif\ifcomment
%\commenttrue # Show comments
\chapter{Theoretical Underpinnings}
\label{chap:theory}
\newthought{In order to best motivate the empirical results} of this thesis and provide a benchmark for classifying big code approaches in future work, we formally pose the \api{} usage matching problem by generalizing the regimen found in our previous work and proving that our setup enjoys several important properties.

\section{\Groums{}}

\begin{definition}
  A \emph{command language} is a set $K$ consisting of \emph{command forms} $k_1, \dots, k_n$.
\end{definition}

\begin{definition}
  An \emph{api signature} is a set $(O, M)$ consisting of a set of object types $O = \{o_1, \dots, o_m\}$ and method signatures $M = \{m_1, \dots, m_n\}$. Each method signature $m_i$ is a tuple $(A_i, o_i, o_{i,r},)$ consisting of:
  \begin{enumerate}
  \item an \emph{argument type tuple} $A_i = \{o_{i,1} \in O \dots o_{i,j} \in O\}$.
  \item a \emph{return type} $o_i \in O$.
  \item a \emph{reciever type} $o_{i,r} \in O$.
  \end{enumerate}
\end{definition}

\begin{definition}
  A \emph{\groum{}} is a labelled graph $(V, E)$, for which:
  \begin{enumerate}
  \item $V$ is a set of nodes.
  \item $E \subseteq V \times V$ is a set of edges.
  \end{enumerate}
  There are three disjoint kinds of node: data nodes $S \subseteq V$, \emph{control nodes} $C \subseteq V$ and \emph{method nodes} $N \subseteq V$. A map $\kappa$ associates with each control node $c \in C$ a command $\kappa(c) \in K$; a map $\tau$ associates with each $s \in S$ an object type $\tau(s) \in O$; and a map $\mu$ associates with each method node $n \in N$ a method signature $\mu(n) \in M$.

  Likewise, there are three disjoint kinds of edge: \emph{control-flow edges} $F \subseteq (C \cup N) \times (C \cup N)$, \emph{use edges} $U \subseteq S \times N$, and \emph{def edges} $D \subseteq N \times C$.
\end{definition}

For convenience, we say that $G' = (V', E')$ is a proper subset of $G = (V, E)$, written $G^{'} \subset G$, if $V^{'} \subset V$ and $E^{'} \subset E$; $G^{'}$ and $G$ are equal, written $G^{'} = G$, if $V^{'} = V$ and $E^{'} = E$; and $G^{'}$ is a (possibly non-strict) subset of $G$, written $G^{'} \subseteq G$, if $G^{'} \subset G$ or $G^{'} = G$.

\begin{definition}
  The \emph{\api{} usage tableau} $\Xi$ enumerates all possible ways to structure \groum{}s to use an \api{} usage signature $(O, M)$ and command language $K$. It's defined as follows:
  \[
    \begin{split}
    \Xi = ( & \{m \in M\}, \{o \in O \}, \{k \in K \}, \\
    & \{(r_1, r_2) | r_1, r_2 \in M \times K \}, \{(m, o) | m \in M, o \in O \}, \{(o, m) | m \in M, o \in O \} )
    \end{split}
    \]
\end{definition}

\section{Embeddings}

\begin{definition}
  Given \groums{} $G_1 = (V_1, E_1)$ and $G_2 = (V_2, E_2)$, an \emph{embedding} $\mathcal{E}(G_1, G_2)$ is a set $(V_\mathcal{E}, E_\mathcal{E})$ consisting of \emph{mapped node-pairs} $V_\mathcal{E} = \{(v_{1,1} \in V_1, v_{1,2} \in V_2), \ldots (v_{p,1} \in V_1, v_{p,2} \in V_2)\}$ and \emph{mapped edge-pairs} $E_\mathcal{E} = \{(e_{1,1} \in E_1, e_{1,2} \in E_2), \ldots, (e_{q,1} \in E_1, e_{q,2} \in E_2)\}$ such that $\mathcal{E}$ is a \emph{partial isomorphism} between $G_1$ and $G_2$: there exist some $G_1' \subseteq G_1$ and $G_2' \subseteq G_2$ such that $\mathcal{E}$ matches only elements of the same kind exactly once. Specifically:
  \begin{enumerate}
  \item data notes in $S_1$ are only mapped to data notes in $S_2$, control nodes in $C_1$ are only mapped to control nodes in $C_2$, and method nodes in $M_1$ are only mapped to method nodes in $M_2$.
  \item control-flow edges in $F_1$ are only mapped to control-flow edges in $F_2$, def edges in $D_1$ are only mapped to def edges in $D_2$, and use edges in $U_1$ are only mapped to use edges in $U_2$, in such a way that the embedding only matches edges between matching nodes: we can have a mapped edge-pair $(e_1, e_2) = ((v_{1,1}, v_{1,2}), (v_{2,1}, v_{2,2})) \in E_{\mathcal{E}}$ only if $(v_{1,1}, v_{1,2}) \in V_{\mathcal{E}}$ and $(v_{2,1}, v_{2,2}) \in V_{\mathcal{E}}$.
  \item Every element in $G_1'$ and $G_2'$ is present in exactly one mapped pair of $\mathcal{E}$: for each $(v_{1,1}, v_{1,2}) \in V_\mathcal{E}$ and $(v_{2,1}, v_{2,2}) \in V_\mathcal{E}$, $v_{1,1} \neq v_{2,1}$ and $v_{1,2} \neq v_{2,2}$; similarly, for each $(e_{1,1}, e_{1,2}) \in E_\mathcal{E}$ and $(e_{2,1}, e_{2,2}) \in E_\mathcal{E}$, $e_{1,1} \neq e_{2,1}$ and $e_{1,2} \neq e_{2,2}$.
  \end{enumerate}
  \label{def:embeddings}
\end{definition}

For an embedding $\mathcal{E}(G_1, G_2)$, we define the following \emph{totality properties}:
\begin{itemize}
\item $\mathcal{E}$ is \emph{left-total}, in which case we may write it as $\mathcal{E}_L$, if $G_1' = G_1$.
\item $\mathcal{E}$ is \emph{right-total}, in which case we may write it as $\mathcal{E}_R$, if $G_2' = G_2$.
\item $\mathcal{E}$ is \emph{total}, in which case we may write it as $\mathcal{E}_{L,R}$, if it's \emph{both} left- and right-total.
\item $\mathcal{E}$ is \emph{properly partial} if it's \emph{neither} left- nor right-total.
\end{itemize}

We also define the following \emph{exactness properties} for $\mathcal{E}(G_1, G_2)$:
\begin{itemize}
\item $\mathcal{E}$ is \emph{exact in method signatures}, in which case we write the embedding as $\mathcal{E}_{N}$, if for each mapped method node pair $(n_1, n_2) \in N_{\mathcal{E}}$, $n_1$ and $n_2$ is associated with the same \api{} method signature: $\mu(n_1) = \mu(n_2)$.
\item $\mathcal{E}$ is \emph{exact in object types}, in which case we write the embedding as $\mathcal{E}_{S}$, if for each mapped data node pair $(s_1, s_2) \in S_{\mathcal{E}}$, $s_1$ and $s_2$ have the same object type: $\tau(s_1) = \tau(s_2)$.
\item $\mathcal{E}$ is \emph{exact in command forms}, in which case we write the embedding as $\mathcal{E}_{C}$, if for each mapped control node pair $(c_1, c_2) \in C_{\mathcal{E}}$, $c_1$ and $c_2$ have the same command form: $\kappa(c_1) = \kappa(c_2)$.
\end{itemize}

\section{Finding Embeddings as a Constraint-Satisfaction Problem}

With embeddings thoroughly defined, we set out to pose the problem of finding an embedding as finding values that satisfy a constraint problem.

For convenience, we define the map $p$ from pairs of elements to Boolean variables representing the given pair appearing in $\mathcal{E}$.

\begin{equation}
  p(v_1, v_2) = (v_1, v_2) \in V_\mathcal{E}
\end{equation}

\begin{equation}
  p(e_1, e_2) = (e_1, e_2) \in E_\mathcal{E}
\end{equation}

Next, we define sets of compatible element pairs whose elements are drawn from $G_1$ and $G_2$. The flag parameters given in double brackets represent imposing exactness constraints on method signatures ($N$), object types ($S$), and control forms ($C$), or left- or right-totality constraints ($L$ or $R$). Informally speaking, including the flag in the identifier on the left-hand side includes the bracketed clause on the right-hand side, with such bracketed clauses labelled by their flags in subscript if there are multiple flags in the left-hand side identifier. Precise meanings for the bracketed terms are given in section~\ref{sec:flag_brac} in appendix~\ref{AppendixA}. The set of all compatible nodes \textsc{compNodes} and set of all compatible edges \textsc{compEdges} are defined as follows:

\begin{equation}
  \begin{split}
  \textsc{compMethodNodes}_{\llbracket N \rrbracket} = \{(n_1, n_2) \in N_1 \times N_2 | \\ \llbracket \mu(n_1) = \mu(n_2) \rrbracket \}
  \end{split}
\end{equation}

\begin{equation}
  \begin{split}
  \textsc{compDataNodes}_{\llbracket S \rrbracket} = \{(s_1, s_2) \in S_1 \times S_2 | \\ \llbracket \tau(s_1) = \tau(s_2) \rrbracket \}
  \end{split}
\end{equation}

\begin{equation}
  \begin{split}
  \textsc{compControlNodes}_{\llbracket C \rrbracket} = \{(c_1, c_2) \in C_1 \times C_2 | \\ \llbracket \kappa(c_1) = \kappa(c_2) \rrbracket \}
  \end{split}
\end{equation}

\begin{equation}
  \begin{split}
  \textsc{compNodes}_{\llbracket N,S,C \rrbracket} = \\ \textsc{compMethodNodes}_{\llbracket N \rrbracket} \cup \textsc{compDataNodes}_{\llbracket S \rrbracket} \cup \textsc{compControlNodes}_{\llbracket C \rrbracket}
  \end{split}
  \label{eqn:cnodes}
\end{equation}

\begin{equation}
  \begin{split}
  \textsc{compFlowEdges}_{\llbracket N,S,C \rrbracket} = \{(f_1 = (v_{1,1}, v_{1,2}), f_2 = (v_{2,1}, v_{2,2})) \in F_1 \times F_2 | \\ (v_{1,1}, v_{2,1}) \in \textsc{compNodes}_{\llbracket N,S,C \rrbracket}, (v_{2,1}, v_{2,2}) \in \textsc{compNodes}_{\llbracket N,S,C \rrbracket} \}
  \end{split}
\end{equation}

\begin{equation}
  \begin{split}
  \textsc{compDefEdges}_{\llbracket N,S,C \rrbracket} = \{(d_1 = (v_{1,1}, v_{1,2}), d_2 = (v_{2,1}, v_{2,2})) \in D_1 \times D_2 | \\ (v_{1,1}, v_{2,1}) \in \textsc{compNodes}_{\llbracket N,S,C \rrbracket}, (v_{2,1}, v_{2,2}) \in \textsc{compNodes}_{\llbracket N,S,C \rrbracket} \}
  \end{split}
\end{equation}

\begin{equation}
  \begin{split}
  \textsc{compUseEdges}_{\llbracket N,S,C \rrbracket} = \{(u_1 = (v_{1,1}, v_{1,2}), u_2 = (v_{2,1}, v_{2,2})) \in U_1 \times U_2 | \\ (v_{1,1}, v_{2,1}) \in \textsc{compNodes}_{\llbracket N,S,C \rrbracket}, (v_{2,1}, v_{2,2}) \in \textsc{compNodes}_{\llbracket N,S,C \rrbracket} \}
  \end{split}
\end{equation}

\begin{equation}
  \begin{split}
    \textsc{compEdges}_{\llbracket N,S,C \rrbracket} = &\\
    \textsc{compFlowEdges}_{\llbracket N,S,C \rrbracket} \cup \textsc{compDefEdges}_{\llbracket N,S,C \rrbracket} & \cup \textsc{compUseEdges}_{\llbracket N,S,C \rrbracket}
  \end{split}
  \label{eqn:cedges}
\end{equation}

For a set $P = \{p_1, \ldots, p_n \}$ of Boolean variables, we now define predicates representing that at least one $p_i$ must be satisfied, that at most one $p_i$ must be satisfied, and that precisely one $p_i$ must be satisfied:
\begin{equation}
  \textsc{atLeastOne}(P) = \bigwedge_{i = 1}^{m} p_i
\end{equation}

\begin{equation}
  \textsc{atMostOne}(P) = \bigvee_{1 \leq i < j \leq m} \neg (p_i \wedge p_j)
\end{equation}

\begin{equation}
  \textsc{exactlyOne}(P) = \textsc{atLeastOne}(P) \wedge \textsc{atMostOne}(P)
\end{equation}

Finally, we define the formula $\psi$ (equation~\ref{eqn:psy}) that the variables $p_i$ representing matches must satisfy to produce an embedding:
  \begin{equation}\psi_{\textsc{compNodes} \llbracket N,S,C \rrbracket } : \bigwedge_{(v_1,v_2) \not\in \textsc{compNode}_{\llbracket N,S,C \rrbracket}} \neg p(v_1, v_2) \label{eqn:psi_cnodes}\end{equation}
  \begin{equation}\psi_{\textsc{compEdges} \llbracket N,S,C \rrbracket } : \bigwedge_{(e_1,e_2) \not\in \textsc{compEdges}_{\llbracket N,S,C \rrbracket}} \neg p(e_1, e_2) \label{eqn:psi_cedges}\end{equation}
  \begin{equation}\psi_{\textsc{leftNode} \llbracket L \rrbracket} : \bigwedge_{v_i \in V_1} \textsc{atMostOne} \llbracket \textsc{exactlyOne} \rrbracket (\{ p(v_i, v_j) | v_j \in V_2 \}) \label{eqn:psi_lnode}\end{equation}
  \begin{equation}\psi_{\textsc{rightNode} \llbracket R \rrbracket} : \bigwedge_{v_j \in V_2} \textsc{atMostOne} \llbracket \textsc{exactlyOne} \rrbracket (\{ p(v_i, v_j) | v_i \in V_1 \}) \label{eqn:psi_rnode}\end{equation}
  \begin{equation}\psi_{\textsc{leftEdge} \llbracket L \rrbracket} : \bigwedge_{e_i \in E_1} \textsc{atMostOne} \llbracket \textsc{exactlyOne} \rrbracket (\{ p(e_i, e_j) | e_j \in E_2 \}) \label{eqn:psi_ledge}\end{equation}
  \begin{equation}\psi_{\textsc{rightEdge} \llbracket R \rrbracket} : \bigwedge_{e_j \in E_2} \textsc{atMostOne} \llbracket \textsc{exactlyOne} \rrbracket (\{ p(e_i, e_j) | e_i \in E_1 \}) \label{eqn:psi_redge}\end{equation}
  \begin{equation}\psi_{\textsc{compEdgeNodes}} : \bigwedge_{e_i = (v_{i,1}, v_{i,2}) \in E_1, e_j = (v_{j,1}, v_{j,2}) \in E_2} p(e_1, e_2) \Rightarrow p(v_{i,1}, v_{j,1}) \wedge p(v_{i,2}, v_{j,2}) \end{equation}
  \begin{equation}\begin{split}
      \psi_{\llbracket L,R;N,S,C \rrbracket} : \psi_{\textsc{compNodes} \llbracket N,S,C \rrbracket } \wedge \psi_{\textsc{compEdges} \llbracket N,S,C \rrbracket } \wedge \psi_{\textsc{leftNode} \llbracket L \rrbracket} \wedge \psi_{\textsc{rightNode} \llbracket R \rrbracket} \\
      \wedge \psi_{\textsc{leftEdge} \llbracket L \rrbracket} \wedge \psi_{\textsc{rightEdge} \llbracket R \rrbracket} \wedge \psi_{\textsc{compEdgeNodes}}
    \end{split}\label{eqn:psy}\end{equation}


\begin{theorem}
  For \groums{} $G_1$ and $G_2$, The variables $P(G_1, G_2)$ maximally satisfying $\psi_{\llbracket N,S,C;L,R \rrbracket}$ defines the \emph{maximal} embedding $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;L,R \rrbracket}(G_1, G_2)$: that is, the embedding respecting all constraints $\llbracket N,S,C; \allowbreak L,R \rrbracket$ that contains the largest numbers of nodes and edges possible common up to equivalence between $G_1$ and $G_2$. Note that as a consequence, $\psi_{\llbracket N,S,C;L,R \rrbracket}$ is satisfiable iff any such embedding exists.
  \label{thm:embed_formula}
\end{theorem}

\begin{proof}
Proceed by comparing the clauses in $\psi$ and definition~\ref{def:embeddings}. Observe in particular that that the left-totality (respectively right-totality) constraint $L$ (respectively $R$) forces equations~\ref{eqn:psi_lnode} and \ref{eqn:psi_lnode} (respectively \ref{eqn:psi_rnode} and \ref{eqn:psi_redge}) to match \emph{every} node and edge in $G_1$ (respectively $G_2$), and that $N$, $S$, and $C$ permit only those matches respecting their exactness constraints in equations~\ref{eqn:psi_cnodes} and~\ref{eqn:psi_cedges} with reference to the functions in equations~\ref{eqn:cnodes} and \ref{eqn:cedges}.
\end{proof}

\section{Compatible Element Sets as Natural Constructions}

In order to define the conditions a \groum{} forces on matching under constraints, we define the \emph{constraint summary} of a \groum{}.

\begin{definition}
  The \emph{\api{} usage matching constraint summary}, of a \groum{} $G$, written $\xi_{\llbracket N,S,C \rrbracket}(G)$, distils how the \groum{} uses signature $(O, M)$ and command language $K$ for the purpose of enforcing constraints $\llbracket N,S,C \rrbracket$ in embedding with other \groum{}s. The matching constraint summary is defined as follows:
  \[
    \begin{split}
      \xi _{\llbracket N, S, C \rrbracket} & (G) = (\\
      &\left\{ m | m \in N, \llbracket m = \mu(n) \textrm{ for some } n \in N \rrbracket_{N}\right\},\\
      &\left\{ o | O \in O, \llbracket o = \tau(s) \textrm{ for some } s \in S \rrbracket_{S}\right\},\\
      &\left\{ k | k \in K, \llbracket k = \kappa(c) \textrm{ for some } c \in C \rrbracket_{C}\right\},\\
      &\{ (r_1, r_2) | r_1, r_2 \in M \times K, \\
      &\quad\llbracket r_1 = \mu(n_1) \textrm{ for some } n_1 \in N \textrm{ if } r_1 \in M, r_2 = \mu(n_2) \textrm{ for some } n_2 \in N \textrm{ if } r_2 \in M \rrbracket_{N},\\
      &\quad\llbracket r_1 = \kappa(c_1) \textrm{ for some } c \in C \textrm{ if } r_1 \in K, r_2 = \kappa(c_2) \textrm{ for some } k \in K \textrm{ if } r_2 \in C \rrbracket_{C}\\
      &\},\\
      &\{ (m, o) | m \in M, o \in O, \llbracket m = \mu(n) \textrm{ for some } n \in N \rrbracket_{N}, \llbracket o = \tau(s) \textrm{ for some } s \in S \rrbracket_{S}\},\\
      &\{ (o, m) | o \in O, m \in M, \llbracket m = \mu(n) \textrm{ for some } n \in N \rrbracket_{N}, \llbracket o = \tau(s) \textrm{ for some } s \in S \rrbracket_{S}\})\\
    \end{split}
  \]
\end{definition}

The next two theorems establish that \textsc{compNodes} and \textsc{compEdges} are \emph{not arbitrary choices} for representing how to match \groum{}s under matching constraints but rather form a \emph{natural construction} over any two \groum{}s $G_1$ and $G_2$ subject to the constraints one chooses to impose in the usage tableau $\Xi_{\llbracket N,S,C \rrbracket}$, in the sense that, up to isomorphism, \textsc{compNodes} and \textsc{compEdges} can be defined uniquely from everything assumed in terms of satisfying a commutative diagram.

\begin{figure}[H]
  \center
  \begin{tikzcd}
  %\begin{tikzpicture}[>=latex]
    \node (v) at (-3,3) {B'};
    \node (w) at (0,0) {B};
    \node (x) at (0,-5) {G_1};
    \node (y) at (5,0) {G_2};
    \node (z) at (5,-5) {\Xi};
    \draw[->] (w) -- (y);
    \draw[->] (w) -- (x);
    \draw[->] (x) -- (z);
    \draw[->] (y) -- (z);
    \draw[->] (v) -- (x);
    \draw[->] (v) -- (y);
    \draw[dashed,->] (v) -- (w);
    \draw (w) -- (x) node [midway, fill=white] {\pi_1};
    \draw (w) -- (y) node [midway, fill=white] {\pi_2};
    \draw (x) -- (z) node [midway, fill=white] {\xi};
    \draw (y) -- (z) node [midway, fill=white] {\xi};
    \draw (v) -- (x) node [midway, fill=white] {\pi_{1}^{'}};
    \draw (v) -- (y) node [midway, fill=white] {\pi_{2}^{'}};
    \draw (v) -- (w) node [midway, fill=white] {f \enskip (\exists !)};
    \begin{scope}[shift=($(w)!.5!(z)$)]
      \draw +(-2,1) -- +(-1,1)  -- +(-1,2);
    \end{scope}
  %\end{tikzpicture}
  \end{tikzcd}
  \caption{Commutative diagram for the pullback $B_{\llbracket N,S,C \rrbracket}$ of $\Xi_{\llbracket N,S,C \rrbracket}$ along $\xi_{\llbracket N,S,C \rrbracket}\allowbreak{}(G_1)$ and $\xi_{\llbracket N,S,C \rrbracket}\allowbreak{}(G_2)$. The commuting square is the subject of theorem~\ref{thm:pullback}, while the alternative pullback $B^{'}_{\llbracket N,S,C \rrbracket}$ and its isomorphism $f$ to $B_{\llbracket N,S,C \rrbracket}$ for which the entire figure commutes is the subject of theorem~\ref{thm:natural}.}
  \label{fig:pullback}
\end{figure}

\begin{theorem}
  The \emph{compatible set} $B_{\llbracket N,S,C \rrbracket} = (\textsc{compNodes}_{\llbracket N,S,C \rrbracket}, \textsc{compEdges}_{\llbracket N,S,C \rrbracket})$ is the pullback of the usage tableau $\Xi_{\llbracket N,S,C \rrbracket}$ along $\xi_{\llbracket N,S,C \rrbracket}(G_1)$ and $\xi_{\llbracket N,S,C \rrbracket}(G_2)$ in the sense that the diagram in figure~\ref{fig:pullback} commutes.
  \label{thm:pullback}
\end{theorem}
\begin{proof}
    Define $B_{1 \llbracket N,S,C \rrbracket}$'s left-projection $\pi_{1}(B_{1 \llbracket N,S,C \rrbracket}) \allowbreak = (V_1 = \{v_1 | (v_1, v_2) \in \textsc{compNodes}_{1 \llbracket N,S,C \rrbracket} \},\allowbreak E_1  = \{e_1 | (e_1, e_2) \in \textsc{compEdges}_{1 \llbracket N,S,C \rrbracket} \}) \allowbreak = G_{1}^{'} \subseteq G_1$ and $B$'s right-projection $\pi_{2}(B_{1 \llbracket N,S,C \rrbracket}) \allowbreak = (V_2 = \{v_2 | (v_1, v_2) \in \textsc{compNodes}_{1 \llbracket N,S,C \rrbracket} \}, \allowbreak E_2  = \{e_2 | (e_1, e_2) \in \textsc{compEdges}_{1 \llbracket N,S,C \rrbracket} \}) = G_{2}^{'} \subseteq G_2$.
    To show theorem~\ref{thm:pullback}, it suffices to show $\xi_{\llbracket N,S,C \rrbracket}(\pi_{1 \llbracket N,S,C \rrbracket}(B)) = \xi_{\llbracket N,S,C \rrbracket}(\pi_{2 \llbracket N,S,C \rrbracket}(B))$. Subject to $\llbracket N,S,C \rrbracket$, $\pi_{1 \llbracket N,S,C \rrbracket}(B)$ gives those nodes and edges in $G_1$ that have equivalent nodes and edges in $G_2$; $\pi_{2 \llbracket N,S,C \rrbracket}(B)$ those nodes and edges in $G_2$ with equivalents in $G_1$. Computing $\xi_{\llbracket N,S,C \rrbracket}$ of either gives the summary of constraining attributes matched on. To check this, observe that $\xi_{\llbracket N,S,C \rrbracket}$ matches all possible \api{} signature entries and command forms, restricting them to those present in the \groum{} itself according to the exactness constraints flagged. As only equivalents are matched, this means $\xi_{\llbracket N,S,C \rrbracket}(\pi_{1 \llbracket N,S,C \rrbracket}(B)) \allowbreak= \xi_{\llbracket N,S,C \rrbracket}(\pi_{2 \llbracket N,S,C \rrbracket}(B))$, as desired.
\end{proof}

\begin{theorem}
  the pullback $B_{\llbracket N,S,C \rrbracket}$ of $\Xi_{\llbracket N,S,C \rrbracket}$ along $\xi_{\llbracket N,S,C \rrbracket}(G_1)$ and $\xi_{\llbracket N,S,C \rrbracket}(G_2)$ given in theorem~\ref{thm:pullback} is \emph{natural} in the sense that for any other pullback $B'_{\llbracket N,S,C \rrbracket}$ with projections $\pi^{'}_{1}(B^{'}_{\llbracket N,S,C \rrbracket})$ and $\pi^{'}_{1}(B^{'}_{\llbracket N,S,C \rrbracket})$ to $G_1$ and $G_2$ through $\xi_{\llbracket N,S,C \rrbracket}(\pi^{'}_{1}(B^{'}_{\llbracket N,S,C \rrbracket})) = \xi_{\llbracket N,S,C \rrbracket}(\pi^{'}_{2}(B^{'}_{\llbracket N,S,C \rrbracket}))$, there exists a unique isomorphism $f$ mapping $B'_{\llbracket N,S,C \rrbracket}$ to $B_{\llbracket N,S,C \rrbracket}$.
  \label{thm:natural}
\end{theorem}
\begin{proof}
  We construct the unique isomorphism. Define the function:
  \[f\left(B^{'}_{\llbracket N,S,C \rrbracket}\right) = \left(\pi^{'}_{1}\left(B^{'}_{\llbracket N,S,C \rrbracket}\right), \pi^{'}_{2}\left(B^{'}_{\llbracket N,S,C \rrbracket}\right)\right)\]

  Now observe that for $B'_{\llbracket N,S,C \rrbracket}$ to be a pullback, $\pi^{'}_{1}\left(B^{'}_{\llbracket N,S,C \rrbracket}\right)$ and $\pi^{'}_{2}\left(B^{'}_{\llbracket N,S,C \rrbracket}\right)$ must be such that $\xi_{\llbracket N,S,C \rrbracket}\left(\pi^{'}_{1}\left(B^{'}_{\llbracket N,S,C \rrbracket}\right)\right) = \xi_{\llbracket N,S,C \rrbracket}\left(\pi^{'}_{2}\left(B^{'}_{\llbracket N,S,C \rrbracket}\right)\right)$, which can be true for the given parts of the diagram (everything but $f$) commuting if $\pi^{'}_{1}\left(B^{'}_{\llbracket N,S,C \rrbracket}\right) = \pi_{1}\left(B_{\llbracket N,S,C \rrbracket}\right)$ and $\pi^{'}_{2}\left(B^{'}_{\llbracket N,S,C \rrbracket}\right) = \pi_{2}\left(B_{\llbracket N,S,C \rrbracket}\right)$, and we therefore have $f\left(B^{'}_{\llbracket N,S,C \rrbracket}\right) = B_{\llbracket N,S,C \rrbracket}$.
\end{proof}

\section{Orders and Connections from \Groum{}s}

In this section, we establish that maximal embeddings under particular constraints can be used to define order- and equivalence-theoretic relations between \groum{}s, from which we can construct the data structures over \groum{}s needed for data mining according to the big code paradigm.

\begin{theorem}
  Every maximal (in the sense of theorem~\ref{thm:embed_formula}) tototality-symmetric (in that sense that either both or neither of constraints $L$ and $R$ are flagged) embedding $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;L,R\rrbracket}(G_1, G_2)$ is symmetric up to equivalence: $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;L,R\rrbracket}(G_1, G_2) \simeq \mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;L,R\rrbracket}(G_2, G_1)$.
  \label{thm:symmetry}
\end{theorem}
\begin{proof}

  Subject to totality-symmetry, $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;L,R\rrbracket}(G_1, G_2)$ matches any nodes $v_1 \in V_1$ with equivalents $v_2 \in V_2$ and any edges $e_1 \in E_1$ with equivalents $e_1 \in E_2$, while $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C\rrbracket}(G_2, G_1)$ matches any nodes $v_2 \in V_2$ with equivalents $v_1 \in V_1$ and any edges $e_2 \in E_2$ with equivalents $e_1 \in E_1$. These two sets of matches are in a one-to-one correspondence with another.
\end{proof}

\begin{theorem}
  Every maximal total embedding $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C\rrbracket L,R }(G_1, G_2)$ defines an equivalence relation $G_1 \simeq G_2$.
\end{theorem}
\begin{proof}
  Every node in $N$ and edge in $E$ trivially maps to itself under any of our constraints, from which we have reflexivity: $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C\rrbracket L,R }(G, G)$ exists.
  The symmetry of $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C\rrbracket L,R }(G_1, G_2)$ is a particular case of theorem~\label{thm:symmetry}.
  To check transitivity, by the definition of totality, $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C\rrbracket L,R}(G_1, G_2)$ exists only if all nodes and edges of $G_1$ are (up to equivalence) present in $G_2$ and vice-versa, and $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;R\rrbracket L}(G_2, G_3)$ exists only if all nodes and edges of $G_2$ are (up to equivalence) present in $G_3$ and vice-versa, so we automatically have the weaker condition that all nodes in $G_1$ are (up to equivalence) present in $G_3$.
\end{proof}

\begin{theorem}
  Every maximal (in the sense of theorem~\ref{thm:embed_formula}) left-total embedding $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;R\rrbracket L}(G_1, G_2)$ defines a well-posed partial order $G_1 \preceq G_2$ that we term the \emph{subsumption relation}.
  \label{thm:poset}
\end{theorem}

\begin{proof}
  To check reflexivity, we note that all nodes and edges in $G_1$ trivially map onto themselves under any constraints, so $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;R\rrbracket L}(G_1, G_2)$ must exist.
  To check transitivity, by the definition of left-totality, $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;R\rrbracket L}(G_1, G_2)$ exists only if all nodes and edges of $G_1$ are (up to equivalence) present in $G_2$, and $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;R\rrbracket L}(G_2, G_3)$ exists only if all nodes and edges of $G_2$ are (up to equivalence) present in $G_3$, so we automatically have the weaker condition that all nodes in $G_1$ are (up to equivalence) present in $G_3$.
  To check antisymmetry, we note that $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;R\rrbracket L}(G_1, G_2)$ must contain all nodes of $G_1$ and will contain all equivalent nodes of $G_2$, and $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;R\rrbracket L}(G_2, G_1)$ must contain all nodes of $G_2$ and all equivalent nodes of $G_1$, which means every node in $G_1$ has an equivalent in $G_2$ and vice-versa, so $G_1$ and $G_2$ are equivalent in the finest sense enforced by the compatibility conditions of $\llbracket N,S,C \rrbracket$ iff maximal $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;R\rrbracket L}(G_1, G_2)$ and $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;R\rrbracket L}(G_2, G_1)$ exist.
\end{proof}

An \emph{API usage cluster}, or simply a \emph{cluster}, is a pair $\mathcal{C} = (\mathcal{G}, \mathcal{E}^{\textrm{max}})$ consisting of the set $\mathcal{G}$ of \groum{}s that make use of a particular \api{}, as well as the maximal embedding partial function $\mathcal{E}^{\textrm{max}}$ of all possible maximal embeddings between them.

Abstractly speaking, we might say that a \emph{cluster model} is some evaluation context $M$ in which judgements $\phi(\mathcal{C})$ can be established, according to the form $M \models \phi(\mathcal{C})$. Put differently, the cluster model gives us a background theory against which to make probability statements about $\mathcal{C}$, our usage cluster. As we are considering applications of \groum{} embedding clusters to data mining, our judgement form should be in terms of probability statements about \emph{events} over $\mathcal{C}$. We won't establish a priori that that our model $M$ respects some probability-theoretic algebraic structure such as a $\sigma$-algebra, for the simple reason that our models are the familiar mathematical structures of lattices and graphs, which have been thoroughly studied from a probabilistic point of view; any such treatment would be neither novel nor contribute significantly towards advancing this thesis.

As a consequence of theorem~\ref{thm:symmetry}, the pair $(\mathcal{G},\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;L,R\rrbracket})$ of \groum{}s $\mathcal{G} = \{G_1, \dots, G_n\}$ and a maximal totality-symmetric embedding $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;L,R\rrbracket}(G_1, G_2)$ defines an undirected graph whose nodes are the graphs $G_i \in \mathcal{G}$, where an edge between $G_1 \in \mathcal{G}$ and $G_2 \in \mathcal{G}$ exists if $\mathcal{E}_{\llbracket N,S,C;L,R\rrbracket}(G_1, G_2)$ exists. We call this graph the \emph{community meta-graph} (\cmg{} for short), since one can construe it as a graph whose nodes are \groum{}s. We have many choices as to the particulars of our \cmg{}, but it warrants mention that making $\mathcal{E}^{\textrm{max}}$ total will make our \cmg{} very uninteresting for the data-mining purposes we wish to consider: such a \cmg{} will consist only of strongly connected cliques of equivalent \groums{}, with no connections between similar but distinct \groums{}. For this reason, every \cmg{} we will consider will impose no totality constraints. In addition, our present \groum{}s lack such sophisticated considerations as object subtyping or semantic/observational equivalence between \groums{}. Likewise, we have several choices for how to weight our edges. A naive choice that we have found to be effective in capturing program similarities is $|V_\mathcal{E}| + |E_\mathcal{E}|$, where $\mathcal{E}^{\textrm{max}}_{\llbracket N,S,C;L,R\rrbracket}(G_1, G_2) = (V_\mathcal{E}, E_\mathcal{E})$.

As a consequence of theorem~\ref{thm:poset}, the pair $(\mathcal{G},\mathcal{E}_{\llbracket N,S,C;R\rrbracket L})$ of \groum{}s $\mathcal{G} = \{G_1, \dots, G_n\}$ and a given maximal left-total embedding $\mathcal{E}_{\llbracket N,S,C;R\rrbracket L}(G_1, G_2)$ defines a lattice, which we term the \emph{subsumption lattice} (\sublat{} for short). This was the structure developed in our previous work for the purposes of identifying common usages in our larger corpus~\cite{fse17}. It is redeveloped here as a consequence of our generalization but is not considered in our experiments to follow in chapter~\ref{chap:experiment}.
