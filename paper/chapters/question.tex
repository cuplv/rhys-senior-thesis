
\begin{savequote}[112mm]
Let's---oh, anything, daddy, so long as it's you and me,\\
And going truly exploring, and not being in till tea!\\
\qauthor{\emph{Just So Stories} by Rudyard Kipling, ``How The Leopard Got His Spots''}
\end{savequote}
   
\chapter{Research Question}
\label{chap:question}

As mentiond in the introduction, the big code data mining community needs high-quality, computable, tractable heuristics for code quality (or \emph{beauty}, which encompasses correctness, clarity and idiomatic usage of programming patterns) to guide its work in efforts like related code search and software synthesis, and frequent occurance of a given usage pattern is a very common heuristic for correctness. As was also noted, however, the literature on this heuristic itself is very sparse compared with how often it's used. This rather naturally leads us to a first question:
\begin{question}
  Is popularity, as considered in terms of frequent occurance of a pattern or similarity to a large number of similar implementations, significant in predicting beauty?
\end{question}

An essential positive hypothesis underlies this question ``in a nutshell''. We term it the \emph{nutshell hypothesis} and pose it as follows:
\begin{hypothesis}
  All other things being equal, the most popular patterns of API usage are the most likely to be correct.
  \label{hyp:nutshell}
\end{hypothesis}

There is a corresponding null hypothesis, namely:
\begin{hypothesis}
  The popularity of API usage has no bearing on correctness.
  \label{hyp:nutnull}
\end{hypothesis}

\section{Desire for Study \& Choice of Approach}
\label{sec:desire}

Attempts to refine this hypothesis further to something directly suitable for an empirical investigation lead us to the following:
\begin{question}
  How can popularity be studied in relation to beauty such that:
  \begin{enumerate}
  \item Our notion of popularity effectively captures what it is for a usage pattern to be common and related to many usage patterns?
  \item The program features we are comparing encode rich strucutral properties we'd expect to appear in a significant number of typical big code projects and experiments?
  \item We can easily gauge whether the usage patterns we're studying are beautiful or not?
  \end{enumerate}
\end{question}

Answering this more elaborate question is challenging on all accounts. We have many choices as to how ``popular'' can be defined. For instance, the most elaborate \api{} usage that encompasses a significant number of other \api{} usages is one admissible notion of popular, while the simpest \api{} usage with which most \api{} usages have some structural similarity is another, yet it's clear that these two notions of popularity may select for wildly different usages in a given corpus of any complexity. The issue of selecting program features and notions of program similarity so as to capture most approaches to big code is more daunting still. Bags of methods~\cite{Michail/Reuse,Zimmerman+Others/2005/Mining, Ying+Others/2004/Predicting,Montandon+Others/2013/Documenting,DBLP:journals/peerj-cs/BorgesV15,Lamba+Others/AndroidTrends}, source-level commands~\cite{long2015staged,long2016automatic}, graph-based control-flow and data models~\cite{Nguyen+Others/2009/Graph} and pre-condition invariants~\cite{Flanagan+Others/Houdini} are among the many kinds of code feature one could collect. Probabilistic log-linear models~\cite{long2016automatic}, frequent itemsets~\cite{Agrawal+Srikant/1994/Fast}, hidden Markov models~\cite{Nguyen+Others/UsagesHMM,Nguyen+Others/RecomendingHMM} and other techniques have all be used to compare program representations to identify related features. Finally, without even considering such subjective properties as clarity and idiomaticity, automatically determining the most objective aspects of beauty, such are correctness, is in general undecidable and a vast area of ongoing research for particular cases. Indeed, this is the reason heuristics like popularity are of interest at all.

Giving even a partial answer to this question demands compromise. The variety of program features, similarity relationships and statistical models one could define makes it effectively impossible to characterize every approach, and it's not possible to generalize results concerning a particular usage to every imaginable way of writing software.

Thankfully, in this thesis, building on work undertaken in our previous writing with colleagues~\cite{fse17}, we define a program model, notion of program similarity, and probabilistic model of program model corpora that we believe is effective in distiling commonalities in usage of large, typically object-oriented \api{}s like the Android \api{} that are widely used and often protocol driven. Because large corpora of open-source apps targeting \api{}s like this exist, and because method invocation-driven usage protocols depend so heavily on syntatic features (eg: the presence or absence of a given \api{}) and very simple semantic features (eg: the results of one method invocation always feeding into another), typically without involving subtleties like pointer arithmetic or numerics for which techniques like seperation logic, refinement typing or numerical domain abstraction are demanded, this is the most applicable setting in which to consider a syntax matching-heavy big code experiment.

In our previous work, we outlined our graph-based program models---a form of \emph{\groum{}}, or program model capturing control flow, data dependency, and method invocation~\cite{Nguyen+Others/2009/Graph}---which effectively capture features common in big code approaches, as well as richer structural features. We also defined an \emph{embedding} between \groum{}s in terms of matching common nodes and edges and show that satisfying a form of constraint problem amounts to finding the largest common embedding. Chapter~\ref{chap:theory} of this thesis reiterates these developments, but in greater generality, giving a set of constraints that can be imposed on the embedding to match different kinds of program functionality. We show that the set of compatible features on which to match are a \emph{natural} as opposed to an arbitrary choice for a given feature constraint tableau, in that they uniquely satisfy a commuting relationship up to isomorphism. We also demonstrate how various choices of embedding give useful structures for relating \groum{}s to one another, culminating in the re-development of the subsumption lattice (\sublat{}) developed in~\cite{fse17} as a special case, as well as the community metagraph (\cmg{}): a \emph{network model} of programs using a given \api{} protocol, where programs are connected by common features.

We claim that the \groum{} is an applicable model of program \api{} usage that strikes a nice balance in preserving expressiveness of features important to \api{} usage without complications that only deeper classical semantic analyses could properly utilize. We also claim that the embedding is a good choice for representing matches between these program models and that the \cmg{} is a suitable context in which to study correctness as a function of popularity, as connections between programs show commonalities, and the relation between the program and the network as a whole can take center stage.

In this thesis, we have decided to confine our definition of beauty to correctness in order to narrow the scope of the experiment that must be performed and test that our representation is robust to implementation variations. Broadly speaking, we wish to study correctness (our choice of beauty) as an effect of popularity (in our case, graph centrality) with our experimental setup.

\section{Testable Hypotheses}

From the considerations of section~\ref{sec:desire}, we now define the simplest hypothesis that our experiment would produce meaningful results in proving: the \emph{weak hypothesis} or \emph{distribution hypothesis}:
\begin{hypothesis}
  The distribution of a centrality-in-community statistic for a set of programs gives significant information about correctness.
  \label{hyp:weak}
\end{hypothesis}

In negating this hypothesis, we derive the corresponding \emph{weak null hypothesis}:
\begin{hypothesis}
  The distribution of a centrality-in-community statistic for a set of programs gives no significant information about correctness.
  \label{hyp:weak_null}
\end{hypothesis}

As a more specific claim than hypothesis~\ref{hyp:weak}, we give the strong form of the \emph{popular is beautiful hypothesis}:
\begin{hypothesis}
  A high centrality-in-community statistic for a given program significantly predicts its correctness.
  \label{hyp:strong}
\end{hypothesis}

For this hypothesis, we define the \emph{strong alternative hypothesis} as its negation:
\begin{hypothesis}
  A high centrality-in-community statistic for a given program does not significantly predict its correctness.
  \label{hyp:strong_alt}
\end{hypothesis}

As a subset of hypothesis~\ref{hyp:strong_alt}, we have the \emph{strong null hypothesis}:
\begin{hypothesis}
  There is no significant difference in likelihood of program correctness given that it has a high centrality-in-community statistic.
  \label{hyp:strong_null}
\end{hypothesis}

We have thus elaborated hypothesis~\ref{hyp:nutshell} and its negation hypothesis~\ref{hyp:nutnull} into two disjoint weaker hypotheses (hypothesis~\ref{hyp:weak} and its negation hypothesis~\ref{hyp:weak_null}) about the distribution and three stronger hypotheses (the disjoint hypotheses~\ref{hyp:strong} and~\ref{hyp:strong_alt}---which is \ref{hyp:strong}'s negation, as well as the latter's subset hypothesis~\ref{hyp:strong_null}) about the general form of the popular is beautiful claim, for a total of five testable hypotheses.

\section{Experimental Plan}

With the background established, we summarize our experiment as follows, with a more thoroughgoing outline to be supplied in chapter~\ref{chap:experiment}. Real-world occurances of a particular usage pattern surrounding the \texttt{SQLiteDatabase} and \texttt{Cursor} \api{}s will be scrutinized in the context of the \cmg{}. This usage has the experimental virtues of being unambiguous in its correctness while providing choice as to how it's used (for instance, how defensive the programmer wishes to be in handling exceptional conditions caused by other parts of the app) as well as ample opportunities to be misused.

First, we manually bin their corresponding \groum{}s for usage examples as either correct or incorrect, without reference to the \cmg{} and the centrality statistic. Second, we compute the empirical distributions of the PageRanks of the \groum{}s with respect to the \cmg{} for each of the correct and incorrect binnings. PageRank is a graph spectrum-based measure of graph centrality that's popular throughout the disciplines of computer and network science~\cite{ilprints422}, so that these distributions will reflect the popularity of a given correct or incorrect usage. Finally, we perform a statistical test of significance to see if the popularity of a correct usage is equal in distribution to the popularity of an incorrect usage. This supplies a robust trial of whether popularity is a significant predictor of correctness. Lastly, we fit two simple statistical models, one continuous and one discrete, to predict correctness given popularity as measured by PageRank as part of a cross-validation scheme, and we quantify their precision and recall across all experiments to test the strong form of the popular is beautiful hypothesis (hypothesis~\ref{hyp:strong}).

With our intentions established, we continue to chapter~\ref{chap:theory}, where we pose our treatment of the big code paradigm and dissertate our approach from first principles.


